To run tests add appropriate input files to test/resources.   
Tests are run with appropriate memory option available within configuration of maven-surefire-plugin in pom.xml.   
[![CircleCI](https://circleci.com/bb/padrian92/phrygia.svg?style=svg)](https://circleci.com/bb/padrian92/phrygia)