package com;

import com.readwrite.process.FileChannelProcessor;
import com.readwrite.write.CsvCreator;
import com.readwrite.write.XmlCreator;

public class App {
    /**
     * arg[0] - xml / csv - choose which file extension you want to produce
     * arg[1] - path to input file
     * arg[2] - path to output file
     * @param args
     */
    public static void main(String[] args) {
        FileChannelProcessor fileProcessor;

        switch (args[0]) {
            case "xml":
                fileProcessor = new FileChannelProcessor(new XmlCreator());
                break;
            case "csv":
            default:
                fileProcessor = new FileChannelProcessor(new CsvCreator());
                break;
        }
        fileProcessor.readAndWrite(args[1], args[2]);
    }
}
