package com.readwrite.write;

import java.util.List;
import java.util.stream.IntStream;

public class CsvCreator implements FileContentCreator {
    public static final int NR_OF_CSV_HEADER_STRINGS = 25;
    private int counter = 0;

    public void appendHeaderToFile(StringBuilder sb) {
        IntStream.range(1, NR_OF_CSV_HEADER_STRINGS + 1).forEach(n -> sb.append(", ").append("Word " + n));
        sb.append("\n");
    }

    public void addSentenceToFile(StringBuilder sb, List<String> sentenceBuffer) {
        counter++;
        sb.append("Sentence" + counter);
        sentenceBuffer.stream().sorted(String::compareToIgnoreCase).forEach(w -> sb.append(", ").append(w));
        sb.append("\n");
    }


    public void appendFooterToFile(StringBuilder sb) {
        // There is no need to add enclosing content in CSV files
    }
}
