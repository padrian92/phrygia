package com.readwrite.write;

import java.util.List;

public interface FileContentCreator {
    void appendHeaderToFile(StringBuilder sb);
    void addSentenceToFile(StringBuilder sb, List<String> sentenceBuffer);
    void appendFooterToFile(StringBuilder sb);
}
