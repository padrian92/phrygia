package com.readwrite.write;

import java.util.List;

public class XmlCreator implements FileContentCreator {
    public void appendHeaderToFile(StringBuilder sb) {
        sb.append("<?xml version='1.0' encoding='UTF-8' standalone='yes'?>");
        sb.append("\n<text>\n");
    }

    public void addSentenceToFile(StringBuilder sb, List<String> sentenceBuffer) {
        sb.append("<sentence>\n");
        sentenceBuffer.stream().sorted(String::compareToIgnoreCase).forEach(s -> sb.append("<word>").append(s).append("</word>"));
        sb.append("\n</sentence>\n");
    }

    public void appendFooterToFile(StringBuilder sb) {
        sb.append("</text>");
    }
}
