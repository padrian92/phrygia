package com.readwrite.write;

import lombok.extern.java.Log;

import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.util.logging.Level;

@Log
public class Writer {

    private Writer() {}

    public static long appendBytesToFile(String fileName, byte[] toBeSaved, long position) {
        try(FileChannel channel = new RandomAccessFile(fileName, "rw").getChannel()) {

            ByteBuffer buffer = channel.map(FileChannel.MapMode.READ_WRITE, position, toBeSaved.length);
            buffer.put(toBeSaved);

        } catch (IOException e) {
            log.log(Level.SEVERE, e.getMessage());
        }
        return position + toBeSaved.length;
    }
}
