package com.readwrite.process;

import com.readwrite.write.FileContentCreator;
import lombok.extern.java.Log;

import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.logging.Level;

import static com.readwrite.write.Writer.appendBytesToFile;

@Log
public class FileChannelProcessor {
    private Set<String> dotAbbreviations = Set.of("Mr", "Ms", "Dr");
    private Set<String> wordSeparators = Set.of(" ", ",", "-");
    private Set<String> sentenceSeparators = Set.of(".", "!", "?");

    private FileContentCreator writer;

    public FileChannelProcessor(FileContentCreator writer) {
        this.writer = writer;
    }

    public void readAndWrite(String inputFilePath, String outputFilePath) {
        List<String> sentenceBuffer = new ArrayList<>();
        ByteBuffer buffer = ByteBuffer.allocate(100000);

        StringBuilder fileContentSB = new StringBuilder();
        StringBuilder wordSB = new StringBuilder();
        long byteBufferPosition = 0;

        try (
                RandomAccessFile aFile = new RandomAccessFile(inputFilePath, "r");
                FileChannel inChannel = aFile.getChannel()
        ) {
            writer.appendHeaderToFile(fileContentSB);

            while (inChannel.read(buffer) > 0) {
                buffer.flip();
                for (int i = 0; i < buffer.limit(); i++) {
                    char ch = (char) buffer.get();
                    String stringCh = String.valueOf(ch);

                    if (wordSeparators.contains(stringCh) || sentenceSeparators.contains(stringCh)) {
                        String word = wordSB.toString().replaceAll("\\s", "");
                        addWordIfItExists(sentenceBuffer, wordSB, word);
                        sentenceBuffer = appendSentenceToFileIfSentenceSeparatorMet(sentenceBuffer, fileContentSB, stringCh, word);
                    } else {
                        wordSB.append(ch);
                    }
                }
                String toBeSaved = fileContentSB.toString();
                byteBufferPosition = appendBytesToFile(outputFilePath, toBeSaved.getBytes(), byteBufferPosition);
                fileContentSB.setLength(0);
                buffer.clear();
            }

            writer.appendFooterToFile(fileContentSB);
        } catch (IOException e) {
            log.log(Level.SEVERE, e.getMessage());
        }
        log.info("File saved!");
    }

    private List<String> appendSentenceToFileIfSentenceSeparatorMet(List<String> sentenceBuffer, StringBuilder fileContentSB, String stringCh, String word) {
        if (sentenceSeparators.contains(stringCh) && !dotAbbreviations.contains(word)) {
                writer.addSentenceToFile(fileContentSB, sentenceBuffer);
                sentenceBuffer = new ArrayList<>();
        }
        return sentenceBuffer;
    }

    private void addWordIfItExists(List<String> sentenceBuffer, StringBuilder wordSB, String wordWithoutWhiteSpaces) {
        if (!wordWithoutWhiteSpaces.isEmpty()) {
            wordSB.setLength(0);
            sentenceBuffer.add(wordWithoutWhiteSpaces);
        }
    }

}