package com.utilities;

public class ResourcePathResolver {
    private ResourcePathResolver() {}

    public static String generatePathToTestResources(String fileName) {
        return System.getProperty("user.dir") + "/src/test/resources/" + fileName;
    }

    public static String generatePathToRootDirectory(String fileName) {
        return System.getProperty("user.dir") + "/" + fileName;
    }
}
