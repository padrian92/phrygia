package com.utilities;

public class Regexes {
    private Regexes() {}
    public static final String WHITESPACES_REGEX = "\\s+";
    public static final String SENTENCE_END_REGEX = "[.!?]+";
    public static final String SENTENCE_END_NO_DELETION_REGEX = "(?<=[.!?]+)";
    public static final String NEGATIVE_LOOKBEHIND_REGEX = "(?<!(?:Mr|Ms|Dr))[.!?]+";
    public static final String NEGATIVE_LOOKBEHIND_NO_DELETION_REGEX = "(?<=(?<!(?:Mr|Ms|Dr))[.!?]+)";
    public static final String WORD_END_REGEX = "[.!?(:),-]+";
}
