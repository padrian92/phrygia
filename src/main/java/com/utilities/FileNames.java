package com.utilities;

public class FileNames {
    private FileNames() {}

    public static final String SMALL_INPUT_FILENAME = "small.in";
    public static final String SMALL_OUTPUT_XML_FILENAME = "my_small.xml";
    public static final String SMALL_OUTPUT_CSV_FILENAME = "my_small.csv";

    public static final String LARGE_INPUT_FILENAME = "large.in";
    public static final String LARGE_OUTPUT_XML_FILENAME = "my_large.xml";
    public static final String LARGE_OUTPUT_CSV_FILENAME = "my_large.csv";

    public static final String OUTPUT_XML_FILENAME = "file.csv";
    public static final String OUTPUT_CSV_FILENAME = "file.csv";
}
