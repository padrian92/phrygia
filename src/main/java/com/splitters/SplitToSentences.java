package com.splitters;

import java.util.Arrays;
import java.util.List;
import java.util.regex.Pattern;

import static com.utilities.Regexes.NEGATIVE_LOOKBEHIND_NO_DELETION_REGEX;

public class SplitToSentences {
    private SplitToSentences() {}

    private static Pattern pattern = Pattern.compile(NEGATIVE_LOOKBEHIND_NO_DELETION_REGEX);

    public static List<String> splitWithoutDeletionOldWay(String fileContent) {
        String[] arr = fileContent.split(NEGATIVE_LOOKBEHIND_NO_DELETION_REGEX);
        return Arrays.asList(arr);
    }

    public static List<String> splitWithoutDeletion(String fileContent) {
        String[] arr = pattern.split(fileContent);
        return Arrays.asList(arr);
    }
}
