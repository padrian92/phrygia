package com.splitters;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import static com.utilities.Regexes.WORD_END_REGEX;
import static com.utilities.Regexes.WHITESPACES_REGEX;

public class SplitToWords {

    private SplitToWords() {}

    public static List<String> split(String sentence) {
        String sentenceWithoutPunctuationMarks = sentence.replaceAll(WORD_END_REGEX, " ");
        List<String> words = new ArrayList<>(Arrays.asList(sentenceWithoutPunctuationMarks.split(WHITESPACES_REGEX)));
        return words.stream().filter(m -> m != null && !"".equals(m)).sorted(String::compareToIgnoreCase).collect(Collectors.toList());
    }
}
