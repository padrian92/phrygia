package com.model;

import javax.xml.bind.annotation.XmlRootElement;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@XmlRootElement(name = "sentence")
public class Sentence {

    public Sentence() {}

    public Sentence(List<String> word) {
        this.setWord(word);
    }

    private List<String> word = new ArrayList<>();

    public List<String> getWord() {
        return word;
    }

    public void setWord(List<String> word) {
        this.word = word.stream()
                .map(s -> s.replaceAll("\\s", ""))
                .sorted(String::compareToIgnoreCase)
                .collect(Collectors.toList());
    }

    public void addWord(String sentence) {
        this.word.add(sentence);
    }

    public String toXMLString() {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("<sentence>\n");
        this.getWord().stream()
                .sorted(String::compareToIgnoreCase)
                .forEach(w -> stringBuilder.append("<word>").append(w).append("</word>"));
        stringBuilder.append("\n</sentence>\n");
        return stringBuilder.toString();
    }

    public String toCSVString(int rowCounter) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Sentence").append(rowCounter + 1);
        this.getWord().stream()
                .sorted(String::compareToIgnoreCase)
                .forEach(w -> stringBuilder.append(", ").append(w));
        return stringBuilder.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Sentence sentence = (Sentence) o;
        return getWord().equals(sentence.getWord());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getWord());
    }
}
