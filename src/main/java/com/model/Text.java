package com.model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.Objects;
import java.util.Set;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "text")
public class Text {

    public Text() {}

    public Text(Set<Sentence> sentences) {
        this.sentences = sentences;
    }

    @XmlElement(name = "sentence")
    private Set<Sentence> sentences;

    public Set<Sentence> getSentences() {
        return sentences;
    }

    public void setSentences(Set<Sentence> sentences) {
        this.sentences = sentences;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Text text = (Text) o;
        return sentences.equals(text.sentences);
    }

    @Override
    public int hashCode() {
        return Objects.hash(sentences);
    }
}
