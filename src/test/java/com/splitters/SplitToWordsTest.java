package com.splitters;

import org.junit.jupiter.api.Test;

import java.util.List;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.assertEquals;

@Deprecated
class SplitToWordsTest {

    public static final String TEXT = "first:second)third,fourth-fifth(sixth";
    public static final List<String> EXPECTED_LIST = List.of("first", "second", "third", "fourth", "fifth", "sixth")
            .stream().sorted(String::compareToIgnoreCase).collect(Collectors.toList());

    @Test
    void splitTest() {
        List<String> words = SplitToWords.split(TEXT);
        assertEquals(EXPECTED_LIST, words, "Splitting was not successful");
    }
}
