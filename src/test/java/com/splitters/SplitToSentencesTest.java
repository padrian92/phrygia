package com.splitters;

import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

@Deprecated
class SplitToSentencesTest {

    public static final int EXPECTED_NR_OF_SENTENCES = 3;

    @Test
    void retrieveSentencesFromFileContentStringWithoutDeletionTest() {
        String sampleString = "Never gonna give you up. Never gonna let you down! Never gonna run around and desert you?";
        List<String> sentences = SplitToSentences.splitWithoutDeletion(sampleString);
        assertEquals(EXPECTED_NR_OF_SENTENCES, sentences.size(), "Wrong number of sentences");
    }

    @Test
    void splittingWithoutPunctuationMarkDeletionTest() {
        //given
        String regex = "(?<=(?<!(?:Mr|Ms|Dr))[.!?]+)";
        String sampleString = "Never gonna give you up. Never gonna let you down! Never gonna run around and desert you?";

        //when
        List<String> sentences = Arrays.asList(sampleString.split(regex));

        //then
        sentences.forEach(s -> assertTrue(
                List.of("!", "?", ".").contains(s.substring(s.length() - 1)),
                "Split was not successful"
        ));
    }
}
