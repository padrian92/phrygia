package com.readwrite.write;

import com.PhrygiaTestWrapper;
import com.model.Sentence;
import com.model.Text;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import java.io.File;
import java.util.List;
import java.util.Set;

import static com.readwrite.write.Writer.appendBytesToFile;
import static com.utilities.FileNames.SMALL_OUTPUT_XML_FILENAME;
import static com.utilities.ResourcePathResolver.generatePathToTestResources;
import static org.junit.jupiter.api.Assertions.assertTrue;

/**
 * Disable extending PhrygiaTestWrapper to see output files
 */
class FileContentCreatorTest extends PhrygiaTestWrapper {

    private FileContentCreator writer;
    private StringBuilder sb = new StringBuilder();
    private JAXBContext jaxbContext;
    private Unmarshaller unmarshaller;

    @BeforeEach
    void setUp() throws JAXBException {
        writer = new XmlCreator();
        jaxbContext = JAXBContext.newInstance(Text.class);
        unmarshaller = jaxbContext.createUnmarshaller();
    }

    @Test
    void deserializationTest() throws JAXBException {
        //given
        Text sampleText = new Text(Set.of(
                new Sentence(List.of("Riders", "on", "the", "storm")),
                new Sentence(List.of("Java", "is", "a", "set", "of", "computer", "software", "and", "specifications",
                        "developed", "by", "James", "Gosling", "at", "Sun", "Microsystems"))
        ));

        //when
        writer.appendHeaderToFile(sb);
        sampleText.getSentences().stream().forEach(s -> writer.addSentenceToFile(sb, s.getWord()));
        writer.appendFooterToFile(sb);
        appendBytesToFile(generatePathToTestResources(SMALL_OUTPUT_XML_FILENAME), sb.toString().getBytes(), 0);

        //then
        Text unmarshalledText =
                (Text) unmarshaller.unmarshal(new File(generatePathToTestResources(SMALL_OUTPUT_XML_FILENAME)));
        assertTrue(unmarshalledText.equals(sampleText), "Unmarshalled Text object is not equal to marshalled object");
    }
}