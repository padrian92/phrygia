package com.readwrite.process;

import com.PhrygiaTestWrapper;
import com.readwrite.write.CsvCreator;
import com.readwrite.write.XmlCreator;
import org.junit.jupiter.api.Test;

import static com.utilities.FileNames.LARGE_INPUT_FILENAME;
import static com.utilities.FileNames.LARGE_OUTPUT_CSV_FILENAME;
import static com.utilities.FileNames.LARGE_OUTPUT_XML_FILENAME;
import static com.utilities.ResourcePathResolver.generatePathToTestResources;
import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;

/**
 * Disable extending PhrygiaTestWrapper to see output files
 * Tests are expected to not throw OutOfMemoryException
 */
class FileChannelProcessorTest extends PhrygiaTestWrapper {

    @Test
    void channelToXMLProcessTest() {
        FileChannelProcessor processor = new FileChannelProcessor(new XmlCreator());
        assertDoesNotThrow(() -> processor.readAndWrite(
                generatePathToTestResources(LARGE_INPUT_FILENAME), LARGE_OUTPUT_XML_FILENAME)
        );
    }

    @Test
    void channelToCSVProcessTest() {
        FileChannelProcessor processor = new FileChannelProcessor(new CsvCreator());
        assertDoesNotThrow(() -> processor.readAndWrite(
                generatePathToTestResources(LARGE_INPUT_FILENAME), LARGE_OUTPUT_CSV_FILENAME)
        );
    }
}