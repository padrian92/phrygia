package com;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

import static com.utilities.FileNames.LARGE_OUTPUT_CSV_FILENAME;
import static com.utilities.FileNames.LARGE_OUTPUT_XML_FILENAME;
import static com.utilities.FileNames.OUTPUT_CSV_FILENAME;
import static com.utilities.FileNames.OUTPUT_XML_FILENAME;
import static com.utilities.FileNames.SMALL_OUTPUT_CSV_FILENAME;
import static com.utilities.FileNames.SMALL_OUTPUT_XML_FILENAME;
import static com.utilities.ResourcePathResolver.generatePathToRootDirectory;
import static com.utilities.ResourcePathResolver.generatePathToTestResources;

public class PhrygiaTestWrapper {
    private List<String> outputFileNames = List.of(SMALL_OUTPUT_XML_FILENAME, SMALL_OUTPUT_CSV_FILENAME,
            LARGE_OUTPUT_XML_FILENAME, LARGE_OUTPUT_CSV_FILENAME, OUTPUT_XML_FILENAME, OUTPUT_CSV_FILENAME);

    @BeforeEach
    void setUp() {
        deleteFiles();
    }

    @AfterEach
    void tearDown() {
        deleteFiles();
    }

    private void deleteFiles() {
        for (String fileName : outputFileNames) {
            deleteFile(generatePathToRootDirectory(fileName));
            deleteFile(generatePathToTestResources(fileName));
        }
        System.out.println();
    }

    private void deleteFile(String fileName) {
        try {
            Files.delete(Paths.get(fileName));
        } catch (IOException e) {}
    }
}
